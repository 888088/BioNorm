import os
import json


def __get_description(idcode,description):
#./leen/leen/
    with open('intact_reactions.json', 'r') as load_s:
        description_sentence=""
        lines_json = load_s.readlines()

        if_find = False 
        for line in lines_json:

            # print(line)
            intact_item = json.loads(line)
            intact_id = intact_item['intact_id']

            if  intact_id == idcode:
                if_find = True 
                description_sentence = intact_item['description']  
                
                description.append(description_sentence)
                return True

       
        if if_find == False:
            print('%s未找到' % (idcode))
            return False



def tran_stat(idcode):

    workdir=os.getcwd()
    chflag=0
    if(workdir[-10:]!="/leen/leen"):
        os.chdir('./leen/leen')
        chflag=1

    description=[]
    __get_description(idcode,description)

    if(chflag==1): 
        os.chdir('../..')
    return description[0]



def predict_sim(idcode,sentence):

    
    workdir = os.getcwd()
    chflag = 0  
    if (workdir[-10:] != "/leen/leen"):
        os.chdir('./leen/leen')
        chflag = 1

    sentence1 = tran_stat(idcode)
    sentence2=sentence.strip("\"")

   
    a = os.popen('th load_model2.lua -task snli ' + '-sent1 \"' + sentence1 + '\" -sent2 \"' + sentence2 + '\"')
   
    lines = a.readlines()
    result=""
    for line in lines:
        if line[:4]=="diff" or line[:4]=="same":
            result=line


    if (chflag == 1):  
        os.chdir('../..')

   
    if (result[:4] == "diff"):
        
        return False
    elif (result[:4] == "same"):
       
        return True
    else:
        print("wrong!")
      
if __name__ == '__main__':

    a=open('source-part.txt','r')
    lines_t=a.readlines()
    cnt=0
    N=0
    for line_t in lines_t:

        # print(N)
        N=N+1
        first_comm=line_t.index(',')
        second_comm=line_t.index(',',first_comm+1)
        idcode_t=line_t[first_comm+1:second_comm]
        sentence_t=line_t[second_comm+1:-1]

        if(print(predict_sim(idcode_t,sentence_t))):
            cnt=cnt+1
    accuracy=cnt/N
    print(accuracy)