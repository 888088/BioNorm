# BioNorm
An implementation of a model described in the following paper for bio-event normalization:

- [BioNorm: Deep learning based event normalization for the curation of reaction databases](https://) by ×××

## Requirements
- [Torch7](https://github.com/torch/torch7) with specifying Lua 5.2 (otherwise you will meet [LuaJIT memory issue](https://github.com/OpenNMT/OpenNMT/issues/26))
- [nn](https://github.com/torch/nn)
- [nngraph](https://github.com/torch/nngraph)
- [optim](https://github.com/torch/optim)
- Python 3.5

## Datasets
- [BioASQvec Plus](https://figshare.com/s/45896c31d10c3f6d857a)

## Usage

Enter your working directory (take `\My Project` for example):
```
cd \MyProject
```

Clone `leen` either by SSH or HTTPS:
```
git clone git@gitlab.com:BioAI/leen.git
git clone https://gitlab.com/BioAI/leen.git
```
If you don't have Git, please install it first as follows:
```
sudo apt-get update
sudo apt-get install git
```


Then import our library by `import` statement:
```python
import leen.leen.test as le
```
You could use our provided functions, with prefixing `le.`. See [FunctionUsage](#functionusage) for more details.
```python
le.predict_sim("EBI-2625447",s1)
le.tran_stat("EBI-2638567") 
```

## FunctionUsage
We have developed two functions:
```python
predict_sim(IntAct ID,text)
```
**Def:** Given an IntAct ID and a text mentioning an event, this function returns True/False indicating whether they refer to the same event 
 
**Parameters:** 

* IntAct ID(str): an accession number of IntAct 
* text: a senctence mentioning an interaction

**Return:**

* A bool value which 'True' indicates the event the text mentions could be normalized to the IntAct ID, while 'False' refers to the opposite  


```python
tran_stat(IntAct ID)
```

**Def:** Given an IntAct ID, it returns the description of this entry

**Parameters:** 

* IntAct ID(str): an accession number of IntAct

**Return:**
A string value which is the description of IntAct ID

# Copyright 
Copyright 2019 . All Rights Reserved.